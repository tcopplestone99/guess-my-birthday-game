import random

#asks user for their name
name = input("Hi! What is your name? ")


#write a function to guess the year and month you were born in while listing the number of attempts that its tried

def guess(x):
    attempts = x
    month = (random.randint(1,12))
    year = (random.randint(1924,2004))
    response = input(("Guess: {}: {} were you born in {}/{} yes or no? ").format(attempts, name, month, year))
    if response.lower() == "yes":
        print("I knew it!")
    elif attempts >= 5:
        print("I have other things to do. Goodbye")
    else:
        attempts += 1
        guess(attempts)

guess(1)
